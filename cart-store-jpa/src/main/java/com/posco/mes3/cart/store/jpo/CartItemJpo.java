package com.posco.mes3.cart.store.jpo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import com.google.gson.Gson;
import com.posco.mes3.cart.domain.entity.CartItem;
import com.posco.mes3.cart.domain.entity.CartProduct;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "TB_CART_ITEM")
@Getter
@Setter
public class CartItemJpo {
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	private int pId;

	@Column(name="QUANTITY") private int quantity;
	
	@Column(name="PRODUCT_INFO") private String productInfo;
	
    @ManyToOne
	private CartJpo cartJpo;
    
    public CartItemJpo() {
		
	}
    
    public CartItemJpo(CartJpo cartJpo,CartItem cartItem) {
		this.cartJpo = cartJpo;
		if(cartItem != null) {
			BeanUtils.copyProperties(cartItem, this);
			this.productInfo = new Gson().toJson(cartItem.getProduct());
		}
	}
	
	public CartItem toDomain() {
		CartProduct product = new Gson().fromJson(this.productInfo, CartProduct.class);
		CartItem cartItem = new CartItem(product,quantity);
		BeanUtils.copyProperties(this, cartItem);
		
		return cartItem;
	}
}
