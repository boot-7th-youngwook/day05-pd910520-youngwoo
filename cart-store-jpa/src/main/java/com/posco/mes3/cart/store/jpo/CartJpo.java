package com.posco.mes3.cart.store.jpo;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.beans.BeanUtils;

import com.posco.mes3.cart.domain.entity.Cart;
import com.posco.mes3.cart.domain.entity.CartItem;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name="TB_CART")
@Getter
@Setter
public class CartJpo {
	
	 	@Id @Column(name="OWNER_ID") private String id;                  // ownerId
	 	
	 	@OneToMany(fetch= FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "cartJpo")
	    private List<CartItemJpo> cartItemJpos;
	 	
	 	public CartJpo() {
	 		if(cartItemJpos == null) {
	 			cartItemJpos = new ArrayList<CartItemJpo>();
	 		}
		}
	 	public CartJpo(String id) {
	 		this();
	 		this.id = id;
	 	}
	 	public CartJpo(Cart cart) {
	 		this();
			BeanUtils.copyProperties(cart, this);
			System.out.println(cartItemJpos.size());
			System.out.println(cart.getItems().size());
			new CartItemJpo(this,null);
			for(CartItem cartItem : cart.getItems()) {
				cartItemJpos.add(new CartItemJpo(this,cartItem));
			}
		}
	 	
	 	public Cart toDomain() {
	 		Cart cart = new Cart(id);
			if(this.cartItemJpos != null ) {
				for(CartItemJpo itemJpo : this.cartItemJpos) {
					cart.addItem(itemJpo.toDomain());
				}
			}
			return cart;
				
		}
}
