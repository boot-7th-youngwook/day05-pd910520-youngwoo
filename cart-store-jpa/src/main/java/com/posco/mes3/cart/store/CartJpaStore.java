package com.posco.mes3.cart.store;

import java.util.NoSuchElementException;
import java.util.Optional;

import org.springframework.stereotype.Repository;

import com.google.gson.Gson;
import com.posco.mes3.cart.domain.entity.Cart;
import com.posco.mes3.cart.domain.store.CartStore;
import com.posco.mes3.cart.store.jpo.CartJpo;
import com.posco.mes3.cart.store.repository.CartItemRepository;
import com.posco.mes3.cart.store.repository.CartRepository;

@Repository
public class CartJpaStore implements CartStore{

	private final CartRepository cartRepository;
	private final CartItemRepository cartItemRepository;
	public CartJpaStore(CartRepository cartRepository,CartItemRepository cartItemRepository) {
		this.cartRepository = cartRepository;
		this.cartItemRepository = cartItemRepository;
	}
	
	@Override
	public void createCart(String id) {
		// TODO Auto-generated method stub
		this.cartRepository.save(new CartJpo(id));
	}

	@Override
	public Cart retrieveCart(String id) {
		// TODO Auto-generated method stub\
		Optional<CartJpo> cartJpo = this.cartRepository.findById(id);
		if( ! cartJpo.isPresent()) {
			throw new NoSuchElementException(String.format("Order(%s) is not found", id));
		}
		return cartJpo.get().toDomain();
	}

	@Override
	public void update(Cart foundCart) {
		// TODO Auto-generated method stub
		this.cartItemRepository.deleteAllInBatch();
		this.cartRepository.save(new CartJpo(foundCart));
	}

	@Override
	public boolean exists(String id) {
		// TODO Auto-generated method stub
		return this.cartRepository.existsById(id);
	}

}
