package com.posco.mes3.cart;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
//import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
//import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
//@EnableEurekaClient
//@EnableFeignClients(basePackages = "io.namoosori")
public class CartApp {
    //
    public static void main(String[] args) {
        //
        SpringApplication.run(CartApp.class, args);
    }
}
