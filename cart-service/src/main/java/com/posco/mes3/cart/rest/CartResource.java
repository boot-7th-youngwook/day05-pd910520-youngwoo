package com.posco.mes3.cart.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.posco.mes3.cart.domain.entity.Cart;
import com.posco.mes3.cart.domain.lifecycle.ServiceLifecycle;
import com.posco.mes3.cart.domain.spec.CartService;

@RestController
public class CartResource {

	private final CartService cartService;
	
	public CartResource(ServiceLifecycle serviceLifecycle){
		this.cartService = serviceLifecycle.requestCartService();
	}

	@GetMapping(value="/register/{ownerId}/{productId}/{quantity}")
	public void registerItem(@PathVariable String ownerId, @PathVariable String productId,@PathVariable int quantity) {
		cartService.registerItem(ownerId, productId, quantity);
	}
	@GetMapping(value="/{ownerId}")
	public Cart findCart(@PathVariable String ownerId) {
		// TODO Auto-generated method stub
		return cartService.findCart(ownerId);
	}
	
	@GetMapping(value="/changeCart/{ownerId}/{productId}/{quantity}")
	public void changeQuantityItem(@PathVariable String ownerId, @PathVariable String productId,@PathVariable int quantity) {
		cartService.changeQuantityItem(ownerId, productId, quantity);
	}

	@GetMapping(value="/remove/{ownerId}/{productId}")
	public void removeItem(@PathVariable String ownerId,@PathVariable String productId) {
		cartService.removeItem(ownerId, productId);
		
	}
	
	
}
