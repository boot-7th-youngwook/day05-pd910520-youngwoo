package com.posco.mes3.cart.lifecycle;

import org.springframework.stereotype.Component;

import com.posco.mes3.cart.domain.lifecycle.ServiceLifecycle;
import com.posco.mes3.cart.domain.spec.CartService;

@Component
public class ServiceLifecycler implements ServiceLifecycle {

	private final CartService cartService;
	
	public ServiceLifecycler(CartService cartService) {
		this.cartService = cartService;
	}
	@Override
	public CartService requestCartService() {
		// TODO Auto-generated method stub
		return this.cartService;
	}

}
