package com.posco.mes3.cart.lifecycle;

import org.springframework.stereotype.Component;

import com.posco.mes3.cart.domain.lifecycle.ProxyLifecycle;
import com.posco.mes3.cart.domain.proxy.ProductProxy;

@Component
public class ProxyLifecycler implements ProxyLifecycle {

	private ProductProxy productProxy;
	
	public ProxyLifecycler(ProductProxy productProxy) {
		this.productProxy = productProxy;
	}
	
	
	@Override
	public ProductProxy requestProductProxy() {
		// TODO Auto-generated method stub
		return this.productProxy;
	}

}
