package com.posco.mes3.cart.bind;

import com.posco.mes3.cart.domain.entity.CartProduct;
import com.posco.mes3.cart.domain.proxy.ProductProxy;
import org.springframework.stereotype.Component;

@Component
public class ProductDelegator implements ProductProxy {
    //
//    private final ProductClient productClient;
//
//    public ProductDelegator(ProductClient productClient) {
//        //
//        this.productClient = productClient;
//    }

//    public CartProduct findProduct(String productId) {
//        //
//        Product product = productClient.findProduct(productId);
//        return new CartProduct(product.getId(), product.getName(), product.getPrice());
//    }

    public CartProduct findProduct(String productId) {
        //
        return new CartProduct("1234", "Productory", 30000);
    }
}
