package com.posco.mes3.cart.logic;

import org.springframework.stereotype.Service;

import com.posco.mes3.cart.domain.lifecycle.ProxyLifecycle;
import com.posco.mes3.cart.domain.lifecycle.StoreLifecycle;
import com.posco.mes3.cart.domain.logic.CartLogic;

@Service
public class CartSpringLogic extends CartLogic {

	public CartSpringLogic(StoreLifecycle storeLifecycle, ProxyLifecycle proxyLifecycle) {
		super(storeLifecycle, proxyLifecycle);
	}

}
