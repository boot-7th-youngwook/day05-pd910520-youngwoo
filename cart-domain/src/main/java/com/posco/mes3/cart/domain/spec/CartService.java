package com.posco.mes3.cart.domain.spec;

import com.posco.mes3.cart.domain.entity.Cart;

public interface CartService {
    //
    public void registerItem(String ownerId, String productId, int quantity);
    public Cart findCart(String ownerId);
    public void changeQuantityItem(String ownerId,  String productId, int quantity);
    public void removeItem(String ownerId, String productId);
}
