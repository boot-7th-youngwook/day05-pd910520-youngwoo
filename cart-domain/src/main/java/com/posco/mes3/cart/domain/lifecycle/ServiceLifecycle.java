package com.posco.mes3.cart.domain.lifecycle;

import com.posco.mes3.cart.domain.spec.CartService;

public interface ServiceLifecycle {

	public CartService requestCartService();
}
