package com.posco.mes3.cart.domain.lifecycle;

import com.posco.mes3.cart.domain.proxy.ProductProxy;

public interface ProxyLifecycle {
	public ProductProxy requestProductProxy();
}
