package com.posco.mes3.cart.domain.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@NoArgsConstructor
public class CartItem {
    //
    private CartProduct product;
    private int quantity;

    public CartItem(CartProduct product, int quantity) {
        //
        this.product = product;
        this.quantity = quantity;
    }

    public CartItem changeQuantity(int quantity) {
        //
        return new CartItem(this.product, quantity);
    }

    public String getProductId() {
        //
        return product.getId();
    }

    public static CartItem sample() {
        //
        return  new CartItem(CartProduct.sample(), 1);
    }
}
