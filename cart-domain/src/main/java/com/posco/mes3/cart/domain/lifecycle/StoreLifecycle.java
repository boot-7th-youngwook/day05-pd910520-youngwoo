package com.posco.mes3.cart.domain.lifecycle;

import com.posco.mes3.cart.domain.store.CartStore;

public interface StoreLifecycle {

	public CartStore requestCartStore();
}
