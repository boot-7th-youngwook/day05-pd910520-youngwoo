package com.posco.mes3.cart.domain.logic;

import com.posco.mes3.cart.domain.entity.CartItem;
import com.posco.mes3.cart.domain.entity.CartProduct;
import com.posco.mes3.cart.domain.lifecycle.StoreLifecycle;
import com.posco.mes3.cart.domain.entity.Cart;
import com.posco.mes3.cart.domain.proxy.ProductProxy;
import com.posco.mes3.cart.domain.lifecycle.ProxyLifecycle;
import com.posco.mes3.cart.domain.spec.CartService;
import com.posco.mes3.cart.domain.store.CartStore;

public class CartLogic implements CartService {
    //
    ProductProxy productProxy;
    CartStore cartStore;

    public CartLogic(StoreLifecycle storeLifecycle, ProxyLifecycle proxyLifecycle) {
        //
        cartStore = storeLifecycle.requestCartStore();
        productProxy = proxyLifecycle.requestProductProxy();
    }

    public void registerItem(String ownerId,  String productId, int quantity) {
        //
        if (!cartStore.exists(ownerId)) {
            cartStore.createCart(ownerId);
        }
        Cart foundCart = cartStore.retrieveCart(ownerId);
        CartProduct product = productProxy.findProduct(productId);
        foundCart.addItem(new CartItem(product, quantity));
        cartStore.update(foundCart);
    }

    public Cart findCart(String ownerId) {
        //
        return cartStore.retrieveCart(ownerId);
    }

    public void changeQuantityItem(String ownerId,  String productId, int quantity) {
        //
    	
        Cart foundCart = cartStore.retrieveCart(ownerId);
        CartProduct product = productProxy.findProduct(productId);

        foundCart.changeQuantity(new CartItem(product, quantity));
        cartStore.update(foundCart);
    }

    public void removeItem(String ownerId, String productId) {
        //
        Cart foundCart = cartStore.retrieveCart(ownerId);
        foundCart.removeItemByProduct(productId);
        System.out.println(foundCart.getItems().size());
        cartStore.update(foundCart);
    }
}
