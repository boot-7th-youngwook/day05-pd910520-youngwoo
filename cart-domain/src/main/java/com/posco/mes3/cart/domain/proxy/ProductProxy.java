package com.posco.mes3.cart.domain.proxy;

import com.posco.mes3.cart.domain.entity.CartProduct;

public interface ProductProxy {
    //
    public CartProduct findProduct(String productId);
}
