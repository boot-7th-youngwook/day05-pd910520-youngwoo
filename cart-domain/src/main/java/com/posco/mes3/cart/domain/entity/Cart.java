package com.posco.mes3.cart.domain.entity;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import lombok.Getter;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

@Getter
public class Cart {
    //
    private String id;                  // ownerId
    private List<CartItem> items;

    public Cart() {
        //
        items = new ArrayList<CartItem>();
    }

    public Cart(String id) {
        //
        this();
        this.id = id;
    }

    public void addItem(CartItem cartItem) {
        //
        if (!this.existsByProduct(cartItem.getProductId())) {
            items.add(cartItem);
        }
    }

    public void changeQuantity(CartItem cartItem) {
        //
        try {
            this.removeItem(cartItem);
            this.addItem(cartItem);
        } catch (NoSuchElementException e) {
            //
        }
    }

    public boolean existsByProduct(String productId) {
        //
        try {
            CartItem cartItem = findCartItem(productId);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    public void removeItem(CartItem cartItem) {
        //
        try {
            CartItem foundItem = this.findCartItem(cartItem);
            System.out.println("removeItem.cartItem::"+cartItem.getProductId());
            System.out.println("removeItem.foundItem::"+foundItem.getProductId());
            items.remove(foundItem);
        } catch (NoSuchElementException e) {
            //
        }
    }

    public void removeItemByProduct(String productId) {
        //
        try {
            CartItem foundItem = this.findCartItem(productId);
            items.remove(foundItem);
        } catch (NoSuchElementException e) {
            //
        }
    }

    private CartItem findCartItem(CartItem cartItem) {
        //
        for (CartItem item: items) {
            if (item.getProduct().getId().equals(cartItem.getProductId())) {
                return item;
            }
        }

        throw new NoSuchElementException();
    }

    private CartItem findCartItem(String productId) {
        //
        for (CartItem item: items) {
            if (item.getProduct().getId().equals(productId)) {
                return item;
            }
        }

        throw new NoSuchElementException();
    }

    public String itemsJson() {
        //
        return (new Gson()).toJson(items);
    }

    public void rebuildItems(String itemsJson) {
        //
        Type listType = new TypeToken<List<CartItem>>() {}.getType();
        this.items = (new Gson()).fromJson(itemsJson, listType);
    }

    public static Cart sample() {
        //
        return new Cart("1234567890");
    }

    public static void main(String[] args) {
        //
        System.out.println((new Gson()).toJson(Cart.sample()));
    }
}
