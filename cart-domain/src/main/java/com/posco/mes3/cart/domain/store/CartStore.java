package com.posco.mes3.cart.domain.store;

import com.posco.mes3.cart.domain.entity.Cart;

public interface CartStore {
    //
    public void createCart(String id);
    public Cart retrieveCart(String id);
    public void update(Cart foundCart);
    public boolean exists(String id);
}
