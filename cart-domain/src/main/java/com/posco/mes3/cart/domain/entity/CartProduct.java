package com.posco.mes3.cart.domain.entity;

import lombok.Getter;

@Getter
public class CartProduct {
    //
    private String id;
    private String name;
    private int price;

    public CartProduct(String id, String name, int price) {
        //
        this.id = id;
        this.name = name;
        this.price = price;
    }

    @Override
    public boolean equals(Object object) {
        //
        if (!(object instanceof CartProduct)) {
            return false;
        }

        CartProduct target = (CartProduct)object;

        return this.id.equals(target.getId());
    }

    public static CartProduct sample() {
        //
        return new CartProduct("0123456789", "laptop", 870000);
    }

}
